# README #
* This application will administer the Canes Camps
* 1.0

### How do I get set up? ###

* Clone the project
* Create the database canes and run the sql in the other sources/db.mysql folder
* Note that this project depends on cisadmin.  It contains web services which expect the wsdl file to be available on localhost.  Make sure cisadmin is up and running before clean/build is completed on canes.


### Who do I talk to? ###

* BJ MacLean
* HCCIS