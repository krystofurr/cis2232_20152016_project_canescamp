/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service.impl;

import info.hccis.admin.model.Attendance;
import info.hccis.canes.dao.repository.AttendanceRepository;
import info.hccis.admin.service.AttendanceService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Evan Cahill
 */
@Service("attendanceService")
public class AttendanceServiceImpl implements AttendanceService {
    
    public final AttendanceRepository attendanceRepo;
  
    @Autowired
    public AttendanceServiceImpl( AttendanceRepository registrationRepo ) {
        this.attendanceRepo = registrationRepo;
    }

    @Override
    public List<Attendance> getAttendances() {
        return (List)attendanceRepo.findAll();
    }

    @Override
    public Attendance getAttendance(int id) {
        return attendanceRepo.findOne(id);
    }
    
    @Override
    public void saveAttendance(Attendance attendance) {
        attendanceRepo.save(attendance);
    }
    
    @Override
    public void saveAttendances(List<Attendance> attendances) {
        attendanceRepo.save(attendances);
    }
    
}
