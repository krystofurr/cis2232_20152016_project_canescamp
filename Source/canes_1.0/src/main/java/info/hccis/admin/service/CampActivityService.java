/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.CampActivity;
import java.util.List;

/**
 *
 * @author krystofurr
 */

public interface CampActivityService {
    
    public abstract List<CampActivity> getCamps(int campID);
    
    public abstract List<CampActivity> getActivities(int activityID);
    
    public abstract List<CampActivity> getCampActivities();
    
    public abstract CampActivity saveCampActivity(CampActivity campActivity);
    
}
