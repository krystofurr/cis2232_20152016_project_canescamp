/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author krystofurr
 */
@Embeddable
public class CampActivityPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "activityId")
    private int activityId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "campId")
    private int campId;

    public CampActivityPK() {
    }

    public CampActivityPK(int activityId, int campId) {
        this.activityId = activityId;
        this.campId = campId;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public int getCampId() {
        return campId;
    }

    public void setCampId(int campId) {
        this.campId = campId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) activityId;
        hash += (int) campId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampActivityPK)) {
            return false;
        }
        CampActivityPK other = (CampActivityPK) object;
        if (this.activityId != other.activityId) {
            return false;
        }
        if (this.campId != other.campId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.CampActivityPK[ activityId=" + activityId + ", campId=" + campId + " ]";
    }
    
}