/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service.impl;

import info.hccis.admin.model.RegistrationSimple;
import info.hccis.canes.dao.repository.RegistrationSimpleRepository;
import info.hccis.admin.service.RegistrationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Evan Cahill
 */
@Service("registrationService")
public class RegistrationServiceImpl implements RegistrationService {
    
    public final RegistrationSimpleRepository registrationRepo;
  
    @Autowired
    public RegistrationServiceImpl( RegistrationSimpleRepository campRepo ) {
        this.registrationRepo = campRepo;
    }

    @Override
    public List<RegistrationSimple> getRegistrations() {
        return (List)registrationRepo.findAll();
    }

    @Override
    public RegistrationSimple getRegistration(int id) {
        return registrationRepo.findOne(id);
    }
    
    
    
}
