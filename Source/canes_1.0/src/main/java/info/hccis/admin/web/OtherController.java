package info.hccis.admin.web;

import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.UserAccess;
import info.hccis.admin.util.Utility;
import info.hccis.admin.web.services.CodeValue;
import info.hccis.admin.web.services.CodeValue_Service;
import info.hccis.admin.web.services.CodeValue_Type;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OtherController {

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

    
    @RequestMapping(value="/other/login",method=RequestMethod.POST)
    public String login(Model model, HttpSession session,@ModelAttribute("login") UserAccess login) {
        /*
        Hit the web service to see if valid credentials.
        */
        //http://bjmac.hccis.info/
//        String urlString = "http://localhost:8080/cisadmin/rest/useraccess/bjmac_canes," + login.getUsername() + "," + login.getPassword();
        String urlString = "http://bjmac.hccis.info:8080/cisadmin/rest/useraccess/bjmac_canes," + login.getUsername() + "," + login.getPassword();
        System.out.println("url for validate=" + urlString);
        String result = Utility.getResponseFromRest(urlString);

        /*
        if successful add token and return to welcome.  Otherwise back to the login.  
        */
        
        if (!result.equals("0")) {
            login.setUserTypeCode(Integer.parseInt(result));
            session.setAttribute("userToken", login);
            return "other/welcome";
        } else {
            model.addAttribute("message","Login failed");
            model.addAttribute("login", login);
            return "other/login";
        }
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {
        
        //check that user is logged in!!!! send back to login.
        
        
        System.out.println("in controller for /");
        //setup the databaseConnection object in the model.  This will be used on the 
        //view.
        DatabaseConnection databaseConnection = new DatabaseConnection();
        model.addAttribute("databaseConnection", databaseConnection);
        CodeValue_Service cvs = new CodeValue_Service();
        CodeValue cv = cvs.getCodeValuePort();
        ArrayList<CodeValue_Type> genderValues = (ArrayList<CodeValue_Type>) cv.list(11, databaseConnection.getDatabaseName(), databaseConnection.getUserName(), databaseConnection.getPassword());
        System.out.println("size of code values=" + genderValues.size());
        ArrayList<CodeValue_Type> grades = (ArrayList<CodeValue_Type>) cv.list(12, databaseConnection.getDatabaseName(), databaseConnection.getUserName(), databaseConnection.getPassword());

        //Store this in the session
        session.setAttribute("GenderTypes", genderValues);
        session.setAttribute("Grades", grades);

        ArrayList<CodeValue_Type> codeValuesBackFromSession = (ArrayList<CodeValue_Type>) session.getAttribute("GenderTypes");
        for (CodeValue_Type cvt : codeValuesBackFromSession) {
            System.out.println(cvt.getEnglishDescription());
        }
       

        UserAccess login = new UserAccess();
        model.addAttribute("login", login);
        model.addAttribute("message","");
        return "other/login";
    }

}
