/*
 *  Entity class to return boolean value from REST service
 */
package info.hccis.admin.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RestResponse {
    
    private boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "RestResponse{" + "result=" + result + '}';
    }
}
