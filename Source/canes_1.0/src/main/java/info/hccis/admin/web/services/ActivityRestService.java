
package info.hccis.admin.web.services;

import info.hccis.admin.model.Activity;
import info.hccis.admin.model.Camp;
import info.hccis.admin.model.CampActivity;
import info.hccis.admin.model.RestResponse;
import info.hccis.admin.service.ActivityService;
import info.hccis.admin.service.CampActivityService;
import info.hccis.admin.service.CampService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author krystofurr
 */


@Component
@Path("/")
@Scope("request") // THIS WILL CAUSE SERVLET TO CRASH IF COMMENTED
@Produces("application/json")
public class ActivityRestService {

    @Resource
    private final ActivityService activityService;
    private final CampService campService;
    private final CampActivityService campActivityService;
    
    private final static Logger LOGGER = Logger.getLogger(ActivityRestService.class.getName());

    
    public ActivityRestService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        
        this.activityService = applicationContext.getBean(ActivityService.class);
        this.campActivityService = applicationContext.getBean(CampActivityService.class);
        this.campService = applicationContext.getBean(CampService.class);
    }
    
    
    /**
     * Save a record in the 'CampActivity' table
     * 
     * @param campActivity
     * @return 
     */
    @POST
    @Path("/save/campActivity")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCampActivity(CampActivity campActivity) {

        // Create a 'RestResponse' class to hold the boolean result value for the client
        RestResponse saveResult = new RestResponse();
        saveResult.setResult(false);
         
        System.out.println("[ REST SERVICE - CampActivity ]: Update composite table with new relationship record");

        CampActivity savedRecord = campActivityService.saveCampActivity(campActivity);
        if(savedRecord != null) {
            saveResult.setResult(true);
        }
        
        return Response.status(200).entity(saveResult).build();
    }
    
    
    
    /**
     * Will provide contents of the composite table between camps and activities.  Also pulls the tables
     * involved in the relationship. Good for loading data into the mobile application with one request
     * 
     * @return 
     */
    @GET
    @Path("/get/camp-activities")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Produces(MediaType.APPLICATION_JSON)
    public List<CampActivity> getCampActivities() {
    
        System.out.println("[ REST SERVICE - Activity ]: Getting all records from CampActivity composite table");
        List<CampActivity> campActivity = campActivityService.getCampActivities();
        if(campActivity == null) {
            LOGGER.severe("This camp does not exist");
        } else {
            return campActivity;
        }
        return null;
    }
    
    /**
     * Will provide all activities that a camp is associated with ( Found with campID )
     * 
     * @return 
     */
    @GET
    @Path("/get/camp/activities/{param}")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Produces(MediaType.APPLICATION_JSON)
    public List<CampActivity> getActivitiesByCamp(@PathParam("param") int campID) {
    
        System.out.println("[ REST SERVICE - Activity ]: Getting all activities based on campID");
        List<CampActivity> campActivity = campActivityService.getCamps(campID);
        if(campActivity == null) {
            LOGGER.severe("This camp does not exist");
        } else {
            return campActivity;
        }
        return null;
    }
    
    /**
     * Will provide all camps that an activity is associated with ( Found with activityID )
     * 
     * @return 
     */
    @GET
    @Path("/get/activity/camps/{param}")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Produces(MediaType.APPLICATION_JSON)
    public List<CampActivity> getCampsByActivity(@PathParam("param") int activityID) {

        System.out.println("[ REST SERVICE - Activity ]: Getting all camps based on activityID");
        List<CampActivity> campActivity = campActivityService.getActivities(activityID);
        if(campActivity == null) {
            LOGGER.severe("This activity does not exist");
        } else {
            return campActivity;
        }
        return null;
    }
    
    
    /**
     * Get all activities
     * 
     * @return 
     */
    @GET
    @Path("/get/activities")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Produces(MediaType.APPLICATION_JSON)
    public List<Activity> getActivities() {

        System.out.println("[ REST SERVICE - Activity ]: Getting all activities");
        List<Activity> activities =  activityService.getActivities();
        if(activities == null) {
            LOGGER.severe("No activities exist");
        } else {
            return activities;
        }
        return null;
    }
    
    
    /**
     * Get all Camps
     * 
     * @return 
     */
    @GET
    @Path("/get/camps")
    // This will automatically call the Jackson JSON dependency when this method is called
    @Produces(MediaType.APPLICATION_JSON)
    public List<Camp> getCamps() {

        System.out.println("[ REST SERVICE - Activity ]: Getting all camps"); 
        List<Camp> camps = campService.getCamps();
        if(camps == null) {
            LOGGER.severe("No camps exist");
        } else {
            return camps;
        }
        return null;
    }
    
    
    /**
     * Get a single activity based on the 'id'
     * 
     * @return 
     */
    @GET
    @Path("/get/activity/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Activity getActivity(@PathParam("param") int id) {
    
        System.out.println("[ REST SERVICE - Activity ]: Getting a single activity with ID: " + id); 
        Activity activity = activityService.getActivity(id);
        if(activity == null) {
            LOGGER.severe(Level.SEVERE + "= No activity with the ID of " + id + " exists.");
        } else {
            return activity;
        }
        return null;
        
    }
    
    
    /**
     * Get a single camp based on the 'id'
     * 
     * @return 
     */
    @GET
    @Path("/get/camp/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Camp getCamp(@PathParam("param") int id) {
    
        System.out.println("[ REST SERVICE - Activity ]: Getting a single camp with ID: " + id); 
        Camp camp = campService.getCamp(id);
        if(camp == null) {
            LOGGER.severe("No camps exist");
        } else {
            return camp;
        }
        return null; 
    }
    
    /**
     * Save single activity.
     * 
     * @param activities
     * @param activity
     * @return 
     */
    @POST
    @Path("/save/activity")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveActivities(Activity activity) {

        System.out.println("[ REST SERVICE - Activity ]: Saving Activity..."); 
        activityService.saveActivity(activity);
        
        return Response.status(200).entity("[ REST SERVICE - Activity ]: Save Successful").build();
          

        
    }
    
    /**
     *  Consumes a list collection of activities and persists them to the database.
     * 
     *  DOES NOT WORK. Possible issue is not with this method but with the client end and
     *  formatting of the JSON.  Leaving to test.
     * 
     * @param activities
     * @return 
     */
    @POST
    @Path("/save/activities")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveActivities(List<Activity> activities) {

        for(Activity activity : activities) {
            System.out.println("Saving => " + activity.toString());
        }
        activityService.saveActivities(activities);
        return Response.status(200).entity("[ REST SERVICE - Activity ]: Save all successful").build();
    }
    
    /**
     *  Update an existing activity by it's ID.  An object is passed in so that the new 
     *  information can be passed through.
     * 
     * @param activity
     * @return 
     */
    @POST
    @Path("/update/activity")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateActivity(Activity activity) {
        
        System.out.println("[ REST SERVICE - Activity ]: Updating Activity..."); 
        activityService.updateActivity(activity.getActivityId(), activity.getName(), activity.getDescription(), 
                                       activity.getRules(), activity.getComments(), activity.getRating(),
                                       activity.getSyncstamp());
 
        return Response.status(200).entity("[ REST SERVICE - Activity ]: Update Successful").build();
    }
    
    /**
     * Deletes an activity based on the activityId
     * 
     * @param id
     * @return 
     */
    @GET
    @Path("/delete/activity/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteActivity(@PathParam("param") int id) {
        
        activityService.deleteActivity(id);
        return Response.status(200).entity("[ REST SERVICE - Activity ]: Deleted Activity").build();
    }
    
    /**
     * Compares 'syncStamp' attributes ( timestamps ) with the remote and mobile records
     * Will take the newest record and save it.
     * 
     * @param activity
     * @return 
     */
    @POST
    @Path("/sync/activity")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response syncActivity(Activity activity) {
        
        Activity resultRecord = null;
        
        System.out.println(activity.toString());
        resultRecord = activityService.syncActivity(activity);
//        String result = "Syncing Activity!";

        return Response.status(200).entity(resultRecord).build();
    }
    
    

    
    
}
