/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.canes.dao.repository;

import info.hccis.admin.model.CampActivity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author krystofurr
 */
@Repository
public interface CampActivityRepository extends CrudRepository<CampActivity, Integer>{
    
    /**
     * Returns all of the records from the composite table
     * 
     * @return 
     */
    @Query("SELECT a FROM CampActivity AS a")
    public abstract List<CampActivity> getCampActivities();
    
    /**
     * Will provide all activities that a camp is associated with.
     * 
     * @param campID
     * @return 
     */
    @Query("SELECT a FROM CampActivity AS a WHERE a.campActivityPK.campId = :campID")
    public abstract List<CampActivity> getCamps(@Param("campID") int campID);
    
    /**
     * Will provide all camps that a single activity is associated with.
     * 
     * @param activityID
     * @return 
     */
    @Query("SELECT a FROM CampActivity AS a WHERE a.campActivityPK.activityId = :activityID")
    public abstract List<CampActivity> getActivities(@Param("activityID") int activityID);

}