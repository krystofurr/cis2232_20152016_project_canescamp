/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.canes.dao.repository;

import info.hccis.admin.model.AppUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author knewcombe
 */
public interface AppUserRepository extends CrudRepository<AppUser, String>{
    
    /**
     * Will find user to login.
     * 
     * @param username
     * @param password
     * @return 
     */
    @Query("SELECT a FROM AppUser AS a WHERE a.username = :username AND a.password = :password")
    public abstract AppUser getUser(@Param("username") String username, @Param("password")String password);
}

